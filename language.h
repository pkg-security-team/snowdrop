/*

   snowdrop - text watermarking and watermark recovery
   ---------------------------------------------------

   Copyright (C) 2002 by Michal Zalewski <lcamtuf@coredump.cx>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   As a special exception, this program may be linked with the
   OpenSSL library, despite that library's more restrictive license.

   Common language engine API.

*/

#ifndef HAVE_LANGUAGE_H
#define HAVE_LANGUAGE_H

#define MAXBUF 4096

#define DOMAIN_WHITE     0	// Whitespaces, comments
#define DOMAIN_GRAMMAR   1	// Grammar changes (and typos)
#define DOMAIN_FORMAT    2	// Formatting, notation
#define DOMAIN_SYNONYMS  3	// Synonyms / name substitution


void  set_original(const char* buf);
void  set_watermarked(const char* buf);
char* get_orig_atom(void);
char* get_water_atom(void);
int   get_storage(const char* orig, const int domain);
char* set_value(const char* orig,int value, const int domain);
int   get_value(const char* orig,const char* water,int* src,int* va,char test);
int   get_water_pos(void);
void  set_water_pos(int x);
char* get_langdesc(void);

// Helpers.
unsigned int md5_importantstuff(void);
void  module_help(void);
void  md5_wrong(void);

#define fatal(x...) exit( ( fputs("[-] FATAL: ",stderr) + \
                            fprintf(stderr,x) + \
                            fputs("\n\n",stderr) ) != 0 )


#define debug(x...) fprintf(stderr,x)                            

#endif /* not HAVE_LANGUAGE_H */
