/*

   snowdrop - text watermarking and watermark recovery
   ---------------------------------------------------
 
   Copyright (C) 2002 by Michal Zalewski <lcamtuf@coredump.cx>
 
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   As a special exception, this program may be linked with the
   OpenSSL library, despite that library's more restrictive license.

   English language (better quality, lower capacity) backend. Done
   as a hack for lang-eng.c.

*/

#define ENG_FINE 1
#include "lang-eng.c"
