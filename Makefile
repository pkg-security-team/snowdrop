#
#  snowdrop - text watermarking and watermark recovery
#  ---------------------------------------------------
#
#  Copyright (C) 2002 by Michal Zalewski <lcamtuf@coredump.cx>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  As a special exception, this program may be linked with the
#  OpenSSL library, despite that library's more restrictive license.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#

VER	= 0.02b
LANG    = eng engf c

BINROOT = /usr/bin/

CFLAGS  = -ggdb -O9 -fomit-frame-pointer -funroll-loops -fexpensive-optimizations \
          -ffast-math -Wall 


all: modules snowdrop toinstall

modules: language.h
	@echo ; \
	echo "[*] Compiling language modules:" ; \
	test -d /usr/include/openssl && USEOPENSSL=1; \
	test -d /usr/local/include/openssl && USEOPENSSL=1; \
	test "$$USEOPENSSL" = "" || echo "[+] Using OpenSSL MD5 modules." ; \
	test "$$USEOPENSSL" = "" && echo "[+] Trying to use RSA MD5 modules." ; \
	for i in $(LANG); do \
	  echo "[+] Building language module for '$$i'..."; \
	  test "$$USEOPENSSL" = "" || ADDME="-DUSE_OPENSSL" ; \
	  $(CC) $$ADDME $(CFLAGS) -c lang-$$i.c -o lang-$$i.o || exit 1; \
	done; \
	echo "[*] Language modules compiled."

snowdrop: snowdrop.c language.h 
	@echo "[*] Compiling main code:"; \
	test -d /usr/include/openssl && USEOPENSSL=1; \
	test -d /usr/local/include/openssl && USEOPENSSL=1; \
	for i in $(LANG); do \
	  echo "[+] Building 'sd-$$i'..." ; \
	  ADDME="-lmd5"; \
	  test "$$USEOPENSSL" = "" || ADDME="-DUSE_OPENSSL -lcrypto" ; \
	  $(CC)  -DVER=\"$(VER)\" $(CFLAGS) -DTARGETLANG=\"$$i\" snowdrop.c lang-$$i.o -o sd-$$i $$ADDME || exit 1; \
	done; \
	echo "[*] Main code compiled."

toinstall:
	@echo "Type 'make install' to install binaries in $(BINROOT)."
	@echo


clean:
	rm -f sd-* *.o core core.* a.out

install: modules snowdrop
	@echo "[*] Installing binaries in $(BINROOT)..."
	cp -f sd-* $(BINROOT)
	@echo "[*] Installing synonyms database..."
	@mkdir /usr/share/snowdrop || true
	cp synonyms /usr/share/snowdrop/
	@echo "[*] Installation complete."

publish: clean
	@ tar cfvz /snowdrop.tgz /snowdrop; \
	  scp -p /snowdrop.tgz lcamtuf@coredump.cx:/export/www/lcamtuf/snowdrop.tgz; \
	  rm -f /snowdrop.tgz
